Kelompok F08 :
| Nama | NRP |
| ---------------------- | ---------- |
| Faiz Haq Noviandra | 5025211132 |
| Nayya Kamila Putri Y | 5025211183 |
| Cavel Ferrari | 5025211198 |

## Problem 1
Terdapat 4 perintah pada soal pertama
1. Menampilkan 5 rank teratas universitas di Jepang
2. Menampilkan FSR terendah dari hasil di atas
3. Menampilkan 10 universitas teratas berdasarkan ger rank
4. Menampilkan universitas dengan kata kunci 'keren'

### Code
```R
#NO A
echo "Top 5 Universities in Japan"
awk -F "," ' /JP/ {print}' Uniranks.csv | head -n 5 | awk -F "," '{print $1, $2}' > sortJapan.csv
cat sortJapan.csv


#NO B
echo "Top 5 Universities in Japan with the Lowest Faculty Student Score"
awk -F "," -v OFS=' | ' '/JP/ {print $1, $3, $2, $9}' Uniranks.csv | sort -t "|" -nk 4 | head -5


#NO C
echo "Top 10 Universities in Japan With the Highest Employment Outcome Rank"
awk -F "," -v OFS=' / ' '/JP/{print $1,$2,$20}' Uniranks.csv | sort -t "/" -nk3 | head -10


#NO D
echo "The Coolest Uni in World"
awk -F "," '/Keren/{print $1,$2}' Uniranks.csv
```
Perintah "awk" digunakan untuk memfilter baris dalam file "Uniranks.csv" yang memiliki kode negara "JP" dan mencetak lima baris pertama menggunakan perintah "head". Kemudian, perintah "awk" lainnya digunakan untuk memilih dua kolom pertama dari hasil filter dan menyimpannya ke dalam file "sortJapan.csv". Lalu, perintah "cat" digunakan untuk mencetak isi dari file "sortJapan.csv" ke dalam konsol.
Perintah “awk” digunakan untuk memilih nomor pertama, ketiga, kedua, dan ke sembilan dari baris dengan kode negara “JP” dalam file “Uniranks.csv”. Lalu perintah “sort” digunakan untuk mengurutkan hasil filter berdasarkan kolom ke empat (FSR).
Perintah “awk” digunakan untuk memilih nomor pertama, kedua, dan ke dua puluh dari baris dengan kode negara “JP”. Perintah “sort” digunakan untuk mengurutkan hasil filter berdasarkan kolom ke tiga. Lalu, perintah “head” digunakan untuk mencetak sepuluh baris pertama hasil pengurutan.
Perintah “awk” digunakan untuk memfilter baris dalam file “Uniranks.csv” yang memiliki kata “Keren” dan mencetak kolom pertama dan kedua filter.

Revisi
Untuk soal B, ada dua jawaban yang masih salah karena filter angka yang salah dalam code. Sedangkan untuk C dan D hasil outputnya masih salah. Namun kami masih ragu dimana letak salahnya karena lupa menanyakan jawaban yang benar.

## Problem 2

Kobeni ingin pergi ke negara terbaik di dunia bernama Indonesia. Akan tetapi karena uang Kobeni habis untuk beli headphone ATH-R70x, Kobeni tidak dapat melakukan hal tersebut.

#### Code

``` R bash
n=$(find . -name '*kumpulan_*' | wc -l)
```

Script ini akan menghitung jumlah direktori yang memiliki nama yang cocok dengan pola "kumpulan_". Jumlah ini kemudian disimpan dalam "n".

```bash
if ((n == 0)); then
  n=1
  mkdir "kumpulan_$n"
else
  n=$((n + 1))
  mkdir "kumpulan_$n"
fi
```

Jika tidak ada direktori yang cocok dengan pola "kumpulan_", maka script ini akan membuat direktori baru dengan nama "kumpulan_1". 
Jika ada, maka script ini akan membuat direktori baru dengan nama "kumpulan_n+1", di mana n adalah jumlah direktori yang sudah ada.

``` R bash
for ((x = 1; x <= jam; x++)); do
  if [[ "$jam" == "00" ]]; then
    x=1
  fi

  file="foto$x.jpg"
  wget -O "$folder/$file" https://dagodreampark.co.id/images/Dieng_2.jpg
done
```

Loop ini akan mendownload gambar-gambar dari sebuah website. Jumlah gambar yang didownload tergantung pada jam saat ini. 

``` bash
y=$(find . -name '*devil_*' | wc -l)
```

Script ini akan menghitung jumlah file yang memiliki nama yang cocok dengan "devil_". Jumlah ini kemudian disimpan dalam variabel "y".

```bash
if [[ "$jam" == "00" ]]; then
  y=$((y + 1))

  if ((y == 1)); then
    zip -r "devil_$y.zip" "$folder"
  else
    zip -r "devil_$y.zip" "$folder"
  fi
fi
```
Jika jam saat ini adalah 00:00, maka script ini akan membuat sebuah file zip yang berisi direktori "kumpulan_n". Nama file zip tersebut akan memiliki format "devil_n.zip", di mana n adalah jumlah file yang sudah ada dengan pola nama "devil_". 
Jika belum ada file yang cocok dengan pola tersebut, maka file zip yang baru dibuat akan diberi nama "devil_1.zip". Jika sudah ada, maka file zip yang baru akan diberi nama "devil_n+1.zip".



## Problem 3
Peter Griffin ingin membuat sebuah sistem register untuk setiap pengguna yang berhasil mendaftar dalam file /users/users.txt. Untuk melakukan ini, ia perlu menambahkan kode dalam script louis.sh. Setelah pengguna berhasil mendaftar, mereka dapat masuk dengan menggunakan script retep.sh. Dalam script retep.sh, informasi login pengguna dicek dengan cara membaca setiap baris dalam file /users/users.txt dan mencocokkan informasi yang dimasukkan oleh pengguna dengan informasi dalam baris tersebut. Jika informasi login cocok, maka pengguna berhasil login. Jika tidak cocok, maka pengguna akan diminta untuk mencoba lagi.

## Point 1
Untuk memastikan password pada register dan login aman, maka ketika proses input passwordnya harus memiliki ketentuan berikut
Minimal 8 karakter
Memiliki minimal 1 huruf kapital dan 1 huruf kecil
Alphanumeric
Tidak boleh sama dengan username
Tidak boleh menggunakan kata chicken atau ernie
## Solusi
Peter Griffin perlu memastikan bahwa password yang dimasukkan memenuhi aturan pembuatan password yang telah ditetapkan. Hal ini dapat dilakukan dengan menambahkan kondisi if-else pada script louis.sh,
## Code
```
echo "silahkan registrasi"
read -p 'Username : ' username
read -p 'Password : ' pass
passlen=`expr "$pass" : '.*'`
passup=${pass^^}
passlow=${pass,,}
if [ $passlen -lt 8 ];
then
        echo "Password kurang dari 8"
else
    if ! [[ "$pass" =~ [^0-9] ]];
    then
        echo "Harus Kombinasi Huruf dan Angka"
    else
        if [ $pass == $passup ];
        then
            echo "Harus mengandung minimal 1 lowercase"
        elif [ $pass == $passlow ];
        then
            echo "Harus mengandung minimal 1 uppercase"
        else
            if [[ "$pass" =~ [^a-zA-Z0-9] ]];
            then
                echo "Bukan Alphanumeric"
            else
                if ! [[ "$pass" =~ [^a-zA-Z] ]];
                then
                    echo "Harus Kombinasi Huruf dan Angka"
                else
                    if [ $pass == $username ];
                    then
                        echo "Password tidak boleh sama dengan Username"
                    else
                        if [[ $pass =~ "chicken" ]];
                        then
                            echo "Mengandung kata terlarang 'chicken'"
                        elif [[ $pass =~ "ernie" ]];
                        then
                            echo "Mengandung kata terlarang 'ernie'"
                        else
                            while IFS=, read -r u p;
                            do
                                if [[ "$u" == "$username" ]];
                                then
                                    echo "$(date +'%y/%m/%d %T') REGISTER: ERROR User already exists" >> log.txt
                                    exit 0
                                fi
                            done < users.txt
                            echo "$username,$pass" >> users.txt
                            echo "$(date +'%y/%m/%d %T') REGISTER: INFO User $username registered successfully" >> log.txt
                        fi
                    fi
                fi
            fi
        fi
    fi
fi
```
## Louis.sh
Kode di atas merupakan script bash yang digunakan untuk melakukan registrasi user dan melakukan validasi terhadap password yang diinputkan oleh user. Berikut adalah penjelasan singkat dari setiap baris kode:

1. Menampilkan pesan "silahkan registrasi".
2. Mengambil input dari user untuk username dan password menggunakan perintah "read".
3. Menghitung panjang password menggunakan ekspresi regular pada variabel "passlen".
4. Mengonversi password menjadi huruf besar (uppercase) dan huruf kecil (lowercase) menggunakan perintah "^^" dan ",," pada variabel  "passup" dan "passlow".
5. Mengecek apakah panjang password kurang dari 8 karakter, jika iya maka mencetak pesan "Password kurang dari 8".
6. Mengecek apakah password hanya terdiri dari angka, jika iya maka mencetak pesan "Harus Kombinasi Huruf dan Angka".
7. Mengecek apakah password hanya terdiri dari huruf besar, jika iya maka mencetak pesan "Harus mengandung minimal 1 lowercase".
8. Mengecek apakah password hanya terdiri dari huruf kecil, jika iya maka mencetak pesan "Harus mengandung minimal 1 uppercase".
9. Mengecek apakah password mengandung karakter selain huruf dan angka (non-alphanumeric), jika iya maka mencetak pesan "Bukan Alphanumeric".
10. Mengecek apakah password hanya terdiri dari huruf, jika iya maka mencetak pesan "Harus Kombinasi Huruf dan Angka".
11. Mengecek apakah password sama dengan username, jika iya maka mencetak pesan "Password tidak boleh sama dengan Username".
12. Mengecek apakah password mengandung kata "chicken" atau "ernie", jika iya maka mencetak pesan "Mengandung kata terlarang 'chicken'" atau "Mengandung kata terlarang 'ernie'".
13. Membaca file "users.txt" menggunakan perulangan "while" dan memeriksa apakah username yang dimasukkan sudah terdaftar dalam file tersebut.
14. Jika username sudah terdaftar, maka mencetak pesan "ERROR User already exists" pada file log.txt dan keluar dari program.
15. Jika username belum terdaftar, maka menambahkan username dan password baru ke file "users.txt" dan mencetak pesan "INFO User $username registered successfully" pada file log.txt.
16. Program selesai dieksekusi.

```echo "silahkan login"
read -p "Username : " username
read -p "Password : " pass


while IFS=, read -r u p;
do
    if [[ "$u" == "$username" && "$p" == "$pass" ]];
    then
        echo "$(date +'%y/%m/%d %T') LOGIN: INFO User $username logged in" >> log.txt
        exit 0
    fi
done < users.txt


echo "$(date +'%y/%m/%d %T') LOGIN: ERROR Failed login attempt on user $username" >> log.txt
```

## Retep.sh
Kode di atas membaca input username dan password dari pengguna. Selanjutnya, kode melakukan iterasi melalui file "users.txt" menggunakan loop while dengan IFS (Internal Field Separator) yang disetel ke koma (','). Pada setiap iterasi, kode memeriksa apakah baris saat ini memiliki nama pengguna dan kata sandi yang cocok dengan yang dimasukkan pengguna. Jika pasangan nama pengguna dan kata sandi cocok, maka kode mencatat log dengan menambahkan waktu login ke file "log.txt" dan keluar dari program. Namun, jika tidak ada pasangan nama pengguna dan kata sandi yang cocok, maka kode mencatat log dengan menambahkan pesan "Failed login attempt" ke file "log.txt"

## Problem4
Johan Liebert yang kalkulatif ingin mencatat log sistem komputernya dengan format tertentu untuk memudahkan analisis dan pemantauan di masa depan. Untuk itu, ia perlu memodifikasi file syslog dengan menambahkan format log yang diinginkan. Format log yang diinginkan dapat disesuaikan dengan kebutuhan Johan Liebert.
## Point1
Backup file log system dengan format jam:menit tanggal:bulan:tahun (dalam format .txt).
## Solusi
Untuk menyelesaikan masalah tersebut, Johan perlu memodifikasi teks agar sesuai dengan langkah-langkah yang sebenarnya dilakukan. Berikut ini adalah modifikasi teksnya. Johan Liebert ingin membuat sebuah file yang namanya terdiri dari informasi tanggal dan waktu saat ini. Untuk melakukan hal ini, ia perlu mendapatkan informasi tanggal dan waktu saat ini menggunakan perintah "date" dan menyimpan informasi tersebut dalam variabel yang sesuai. Setelah itu, ia akan menggabungkan informasi tersebut menjadi satu string dan menyimpannya dalam variabel "filename". Format nama file yang diinginkan adalah "tahun-bulan-tanggal_jam:menit.log", sehingga ia perlu menyesuaikan urutan format yang diminta dengan informasi yang diperoleh dari perintah "date".
## Code
```
#! /bin/bash


# truncate -s 0 /var/log/syslog


# setting file ke recent log dan mengambil jamnya
filePath="$(ls | awk '/.txt/' | tail -n 1)"
getHour=$(ls | awk '/.txt/' | sort -t: -k1 | tail -n 1 | cut -d':' -f1)


# alfabet yang akan di dekripsi
decryptedArtifacts="abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"


# fungsi dekripsi
function decrypt () {
# Local declaration
  local agentOfDecryption=$1
  local temp=""


# memisahkah upercase dan lowercase
  batasAtas="$(echo ${decryptedArtifacts:26:51})"
  batasBawah="$(echo ${decryptedArtifacts:0:26})"


# mengambil range dari setiap kasus dengan konsep substring  {string:starting:population}
  temp+="${batasBawah:agentOfDecryption%26:26}${batasBawah:0:agentOfDecryption%26}"
  temp+="${batasAtas:agentOfDecryption%26:26}${batasAtas:0:agentOfDecryption%26}"
  echo $temp
}


# mengimplementasi fungsi dekripsi
function ThyFullDecryptionPack () {
  echo -n "$(cat "$filePath" |  r [$(decrypt $getHour)] [$(echo $decryptedArtifacts)])"
}


# memanggil decrypt pack
echo -n "$(ThyFullDecryptionPack)"
```

## decrypt.sh
Kode tersebut merupakan sebuah Bash script yang melakukan dekripsi terhadap sebuah file teks yang disimpan pada variabel filePath. Proses dekripsi dilakukan dengan memanggil fungsi ThyFullDecryptionPack() yang kemudian memanggil fungsi decrypt() untuk memecahkan pesan enkripsi dengan memanfaatkan jam dari file yang telah diambil. Fungsi decrypt() sendiri merupakan fungsi yang berfungsi untuk melakukan proses dekripsi dengan mengubah setiap karakter dalam pesan enkripsi menjadi karakter lain berdasarkan jam yang telah diambil.

Variabel decryptedArtifacts berisi alfabet untuk melakukan dekripsi, baik untuk huruf kecil maupun huruf besar. Kemudian fungsi decrypt() memecah setiap huruf pada pesan enkripsi menjadi huruf lain pada alfabet berdasarkan jam yang diambil, yang kemudian dikembalikan oleh fungsi ThyFullDecryptionPack() dengan menggunakan perintah cat pada file yang telah diambil sebelumnya, kemudian mengeksekusi perintah r dari perintah tr (translate or delete characters) pada Bash, dan seluruh huruf yang ditemukan pada file yang diambil akan diubah menjadi hasil dekripsi yang telah dilakukan pada fungsi decrypt()

``` #! /bin/bash


# setting SYSLOG path di arahLog variabel
arahLog="/var/log/syslog"


# Alphabets yang akan dienkripsi dan di cipher
encryptedArtifacts="abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"


# fungsi enkripsi
function encrypt () {
# Local declaration
  local agentOfEncryption=$1
  local temp=""


# Memisahkan lowercase dan uppercase
  batasAtas="$(echo ${encryptedArtifacts:26:51})"
  batasBawah="$(echo ${encryptedArtifacts:0:26})"


# mendapatkan range dari setiap kasus menggunakan konsep substring  {string:startingArtifact:listOfArtifacts}
  temp+="${batasBawah:agentOfEncryption%26:26}${batasBawah:0:agentOfEncryption%26}"
  temp+="${batasAtas:agentOfEncryption%26:26}${batasAtas:0:agentOfEncryption%26}"
  echo $temp
}


# mengimplementasi fungsi enkripsi di arahlog inner text
function ThyFullEncryptionPack () {
  local filename="$(date +"%H:%M %d:%m:%Y").txt"
  echo -n "$(cat $arahLog | tr [$(echo $encryptedArtifacts)] [$(encrypt $(date +"%H"))] >> "$filename")"
}


# mengenkripsi secara full syslog dan mencadangkan data ke txt fileF
echo -n "$(ThyFullEncryptionPack)"
```

## encrypt.sh
Kode di atas adalah sebuah script bash yang berfungsi untuk mengenkripsi teks di dalam file syslog dengan menggunakan teknik substitution cipher. Berikut adalah penjelasan singkat dari tiap-tiap bagian kode:

1. Baris 3: variabel arahLog didefinisikan sebagai path menuju file syslog yang akan dienkripsi.
2. Baris 6: variabel encryptedArtifacts berisi huruf-huruf yang akan dienkripsi dan di-dekripsi.
3. Baris 9-24: fungsi encrypt menerima satu argumen, yaitu bilangan bulat yang digunakan sebagai kunci enkripsi. Fungsi ini memisahkan huruf-huruf di dalam encryptedArtifacts menjadi huruf-huruf uppercase dan lowercase, kemudian memutar setiap kelompok huruf sebanyak agentOfEncryption kali. Fungsi ini mengembalikan string yang berisi alfabet yang sudah diacak sesuai dengan kunci enkripsi.
4. Baris 27-33: fungsi ThyFullEncryptionPack membaca isi dari file syslog, mengenkripsi teks di dalamnya menggunakan fungsi encrypt, dan menyimpan hasilnya ke dalam file dengan nama yang menunjukkan waktu saat file tersebut dibuat. Fungsi ini tidak mengembalikan nilai apa pun.
5. Baris 36: script utama mengeluarkan hasil dari pemanggilan fungsi ThyFullEncryptionPack tanpa menambahkan karakter newline





